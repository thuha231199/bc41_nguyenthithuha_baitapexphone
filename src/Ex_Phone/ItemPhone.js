import React, { Component } from "react";

class ItemPhone extends Component {
  render() {
    let { hinhAnh, giaBan, tenSP } = this.props.data;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button className="btn btn-success">Xem chi tiết</button>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemPhone;
